package com.example.nosqlexam.service;

import com.example.nosqlexam.entity.NewsRedis;
import com.example.nosqlexam.repository.NewsRedisRepository;
import com.example.nosqlexam.salon.NewsSaveRequest;
import com.example.nosqlexam.salon.NewsSaveResponse;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ReplicationService {

    private final NewsService newsService;
    private final NewsRedisRepository newsRedisRepository;
    private final ModelMapper modelMapper = new ModelMapper();


    public NewsSaveResponse save(@Valid NewsSaveRequest request) {
        NewsSaveResponse saved = newsService.save(request);
        newsRedisRepository.save(modelMapper.map(saved, NewsRedis.class));
        return saved;
    }
}
