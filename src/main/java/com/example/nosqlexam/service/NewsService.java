package com.example.nosqlexam.service;

import com.example.nosqlexam.entity.NewsMongo;
import com.example.nosqlexam.repository.NewsMongoRepository;
import com.example.nosqlexam.salon.NewsResponse;
import com.example.nosqlexam.salon.NewsSaveRequest;
import com.example.nosqlexam.salon.NewsSaveResponse;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class NewsService {

    private final NewsMongoRepository newsMongoRepository;
    private final ModelMapper modelMapper = new ModelMapper();

    public NewsSaveResponse save(@Valid NewsSaveRequest request) {
        NewsMongo map = modelMapper.map(request, NewsMongo.class);
        return new NewsSaveResponse(newsMongoRepository.save(map).getId());
    }

    public NewsResponse findById(String id) {
        return newsMongoRepository.findById(id)
                .map(newsMongo -> modelMapper.map(newsMongo, NewsResponse.class))
                .orElseThrow(IllegalArgumentException::new);
    }

    public void delete(String id) {
        newsMongoRepository.findById(id)
                .ifPresentOrElse(emp -> {
                    newsMongoRepository.deleteById(id);
                }, () -> {
                    throw new IllegalArgumentException();
                });
    }
}
