package com.example.nosqlexam.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;

@Data
@Document(collection = "news")
public class NewsMongo {

    @Id
    private String id = UUID.randomUUID().toString();
    private String name;
    private String header;
    private String payload;
    private String redactorName;

    private String createTime;
    private String approveTime;

    private String a;
    private String b;
    private String c;
    private String d;

}
