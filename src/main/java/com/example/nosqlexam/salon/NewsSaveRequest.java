package com.example.nosqlexam.salon;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class NewsSaveRequest {

    @NotBlank
    private String name;
    @NotBlank
    private String header;
    @NotBlank
    private String payload;
    @NotBlank
    private String redactorName;

    private String createTime;
    private String approveTime;

    private String a;
    private String b;
    private String c;
    private String d;

}
