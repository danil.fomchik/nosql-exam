package com.example.nosqlexam.salon;

public record NewsSaveResponse(String id) {
}
