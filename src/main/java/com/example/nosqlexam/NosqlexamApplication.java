package com.example.nosqlexam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NosqlexamApplication {

	public static void main(String[] args) {
		SpringApplication.run(NosqlexamApplication.class, args);
	}

}
